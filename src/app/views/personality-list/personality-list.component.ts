import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../../core/services/store.service';
import { Observable } from 'rxjs';
import { Personality } from '../../../core/models/personality.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personality-list',
  templateUrl: './personality-list.component.html',
  styleUrl: './personality-list.component.scss'
})
export class PersonalityListComponent implements OnInit {
  personalities$!: Observable<Personality[]>;
  constructor(private storeService: StoreService, private router: Router) { }

  ngOnInit(): void {
    this.loadPersonalities();
  }

  loadPersonalities() {
    this.personalities$ = this.storeService.getPersonalities();
  }

  editPersonality(personality: Personality) {
    this.router.navigate(['/personality-form', personality.id]);
  }

  deletePersonality(personalityId: number) {
    this.storeService.deletePersonality(personalityId);
  }
}
