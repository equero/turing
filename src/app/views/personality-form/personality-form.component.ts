import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Personality } from '../../../core/models/personality.model';
import { Characteristic } from '../../../core/models/characteristic.model';
import { MatChipEditedEvent, MatChipInputEvent } from '@angular/material/chips';
import {COMMA, ENTER, T} from '@angular/cdk/keycodes';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { StoreService } from '../../../core/services/store.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-personality-form',
  templateUrl: './personality-form.component.html',
  styleUrl: './personality-form.component.scss'
})
export class PersonalityFormComponent implements OnInit {
  personalityForm!: FormGroup;
  chars: string[] = [];
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  characteristics: Characteristic[] = [];
  isEdit = false;
  personalityId: number | undefined;

  announcer = inject(LiveAnnouncer);
  constructor(private formBuilder: FormBuilder, private storeService: StoreService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initForm();
    this.checkEditMode();
  }

  private checkEditMode(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.isEdit = true;
        this.personalityId = +params['id'];
        this.loadPersonalityData(this.personalityId);
      }
    });
  }

  private loadPersonalityData(id: number): void {
    this.storeService.getPersonalityById(id).subscribe(personality => {
      if (personality) {
        this.personalityForm.patchValue(personality);
        this.characteristics = personality.characteristics.map(name => ({ name }));
      }
    });
  }

  private initForm(): void {
    this.personalityForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      characteristics: this.formBuilder.array([]),
      use_case: ['', Validators.required],
      status: [true, Validators.required],
      image: ['', Validators.required],
      instructions_count: [0]
    });
  }

  onSubmit(): void {
    console.log(this.personalityForm.valid);
    if (this.personalityForm.valid) {
      const newPersonality: Personality = this.personalityForm.value;
      newPersonality.characteristics = this.characteristics.map(c => c.name)
      this.storeService.createPersonality(newPersonality);
      this.router.navigate(['/personality-list']);
    } else {
      this.personalityForm.markAllAsTouched();
    }
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.characteristics.push({name: value});
    }
    event.chipInput!.clear();
  }

  remove(characteristic: Characteristic): void {
    const index = this.characteristics.indexOf(characteristic);

    if (index >= 0) {
      this.characteristics.splice(index, 1);

      this.announcer.announce(`Removed ${characteristic}`);
    }
  }

  edit(characteristic: Characteristic, event: MatChipEditedEvent) {
    const value = event.value.trim();
    if (!value) {
      this.remove(characteristic);
      return;
    }
    const index = this.characteristics.indexOf(characteristic);
    if (index >= 0) {
      this.characteristics[index].name = value;
    }
  }
}
