import { Component, OnInit } from '@angular/core';
import { StoreService } from '../core/services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'turing-challenge-front-tc';
  openedSideNav: boolean = false;
  isMobile: boolean = false;

  constructor(private storeService: StoreService) { }

  ngOnInit(): void {
    this.checkIsMobile();
  }

  checkIsMobile() {
    if (typeof window !== "undefined") {
      const screenWidth = window.innerWidth;
      this.isMobile = screenWidth < 768;
      this.openedSideNav = this.isMobile ? false : true;
   }
  }
}
