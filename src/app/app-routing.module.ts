import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonalityListComponent } from './views/personality-list/personality-list.component';
import { PersonalityFormComponent } from './views/personality-form/personality-form.component';

const routes: Routes = [
  { path: 'personality-list', component: PersonalityListComponent },
  { path: 'personality-form', component: PersonalityFormComponent },
  { path: 'personality-form/:id', component: PersonalityFormComponent },
  { path: '', redirectTo: '/personality-list', pathMatch: 'full' }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
