import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DataService } from './data.service';
import { Personality } from '../models/personality.model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private personalitiesSubject = new BehaviorSubject<Personality[]>([]);

  constructor(private dataService: DataService) { 
    this.fetchData();
  }

  private fetchData() {
    this.dataService.getPersonalities().subscribe(personalities => {
      this.personalitiesSubject.next(personalities);
    });
  }

  getPersonalityById(id: number): Observable<Personality> {
    return this.dataService.getPersonalityById(id);
  }

  getPersonalities(): Observable<Personality[]> {
    return this.personalitiesSubject.asObservable();
  }

  createPersonality(personality: Personality) {
    this.dataService.createPersonality(personality).subscribe(newPersonality => {
      const updatedPersonalities = [...this.personalitiesSubject.getValue(), newPersonality];
      this.personalitiesSubject.next(updatedPersonalities);
    });
  }

  editPersonality(personality: Personality) {
    this.dataService.editPersonality(personality).subscribe(updatedPersonality => {
      const personalities = this.personalitiesSubject.getValue();
      const index = personalities.findIndex(p => p.id === updatedPersonality.id);
      if (index !== -1) {
        personalities[index] = updatedPersonality;
        this.personalitiesSubject.next([...personalities]);
      }
    });
  }

  deletePersonality(personalityId: number) {
    this.dataService.deletePersonality(personalityId).subscribe(() => {
      const personalities = this.personalitiesSubject.getValue();
      const updatedPersonalities = personalities.filter(p => p.id !== personalityId);
      this.personalitiesSubject.next(updatedPersonalities);
    });
  }
}
