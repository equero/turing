import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';
import { Personality } from '../models/personality.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getPersonalities(): Observable<Personality[]> {
    return this.http.get<Personality[]>(`${environment.apiUrl}/personality`);
  }

  getPersonalityById(id: number): Observable<Personality> {
    return this.http.get<Personality>(`${environment.apiUrl}/personality/${id}`);
  }

  createPersonality(personality: Personality): Observable<Personality> {
    return this.http.post<Personality>(`${environment.apiUrl}/personality`, personality);
  }

  editPersonality(personality: Personality): Observable<Personality> {
    return this.http.put<Personality>(`${environment.apiUrl}/personality/${personality.id}`, personality);
  }

  deletePersonality(personalityId: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/personality/${personalityId}`);
  }
}
