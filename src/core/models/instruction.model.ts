export interface Instruction {
  id: number;
  personality_id: number;
  content: string;
  type: string;
}
