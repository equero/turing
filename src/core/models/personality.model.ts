export interface Personality {
  id: number;
  name: string;
  description: string;
  characteristics: string[];
  use_case: string;
  status: boolean;
  image: string;
  instructions_count: number;
}
